package com.david.youtubetest.manager;

import android.util.Log;

import com.david.youtubetest.model.PlayList;
import com.david.youtubetest.server.ServerConnector;
import com.david.youtubetest.server.request.RequestGetPlayLists;
import com.david.youtubetest.server.response.ResponseGetPlayLists;

import java.util.List;

/**
 * Created by SergataDev on 9/2/2017.
 */
public class DataManager {
    private static String TAG = "DataManager";
    private static DataManager ourInstance = new DataManager();

    private List<PlayList> mPlaylists;

    public static DataManager getInstance() {
        return ourInstance;
    }

    private DataManager() {
    }

    /**
     * fetch playlists in the background and retuen result to listener
     * if the data already fetched return immediately
     * @param listener - for request result
     */
    public void getPlaylists(ServerConnector.OnResultListener listener) {
        if (mPlaylists == null) {
            ServerConnector.getInstance().processRequest(new RequestGetPlayLists(), listener);
        }
        else {
            Log.d(TAG, "playlist has items count " + mPlaylists.size());
            listener.onSuccess(new ResponseGetPlayLists(mPlaylists));
        }
    }

    public void setPlaylists(List<PlayList> playlists) {
        mPlaylists = playlists;
    }

}
