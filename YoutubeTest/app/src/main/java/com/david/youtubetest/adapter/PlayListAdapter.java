package com.david.youtubetest.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.david.youtubetest.viewholder.ListItemViewHolder;
import com.david.youtubetest.viewholder.PlayListViewHolder;
import com.david.youtubetest.R;
import com.david.youtubetest.model.PlayList;

import org.kaerdan.twolevelexpandablerecyclerview.TwoLevelExpandableAdapter;
import org.kaerdan.twolevelexpandablerecyclerview.ViewHolderWithSetter;

import java.util.List;

/**
 * Created by SergataDev on 9/2/2017.
 */

public class PlayListAdapter extends TwoLevelExpandableAdapter<PlayList> {
    public PlayListAdapter(List<PlayList> playlists) {
        super(playlists);
    }

    @Override
    public ViewHolderWithSetter getSecondLevelViewHolder(ViewGroup parent) {
        return new ListItemViewHolder(View.inflate(parent.getContext(),
                R.layout.entry_list_item, null));
    }

    @Override
    public ViewHolderWithSetter getTopLevelViewHolder(ViewGroup parent) {
        return new PlayListViewHolder(View.inflate(parent.getContext(),
                R.layout.entry_playlist_item, null));
    }
}
