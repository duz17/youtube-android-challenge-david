package com.david.youtubetest.server.response;

import com.david.youtubetest.manager.DataManager;
import com.david.youtubetest.model.PlayList;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetPlayLists extends BaseResponse {

    @SerializedName("Playlists")
    private List<PlayList> playlists;

    public ResponseGetPlayLists(){}
    public ResponseGetPlayLists(List<PlayList> playlists){
        this.playlists = playlists;
    }

    public List<PlayList> getData() {
        return playlists;
    }

    @Override
    public void onResponseDoOnBackground() {
        DataManager.getInstance().setPlaylists(playlists);
    }
}