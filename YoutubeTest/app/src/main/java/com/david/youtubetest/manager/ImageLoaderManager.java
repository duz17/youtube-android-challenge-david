package com.david.youtubetest.manager;

import android.content.Context;
import android.graphics.Bitmap;

import com.david.youtubetest.MyApplication;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

/**
 * singleton that configures ImageLoader for all application
 */
public class ImageLoaderManager {
 
    private static ImageLoader imageLoader;

    public static ImageLoader getInstance(){
        if (imageLoader == null) {
            imageLoader = ImageLoader.getInstance();
 
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(MyApplication.getContext())
                    .memoryCacheExtraOptions(480, 800)
                    .diskCacheExtraOptions(480, 320, null)
                    .threadPoolSize(7)
                    .threadPriority(Thread.MIN_PRIORITY + 2)
                    .diskCacheSize(50 * 1024 * 1024)
                    .diskCacheFileCount(100)
                    .denyCacheImageMultipleSizesInMemory()
                    .memoryCache(new LruMemoryCache(7 * 1024 * 1024))
                    .memoryCacheSize(7 * 1024 * 1024) // 7 Mb
                    .imageDownloader(new BaseImageDownloader(MyApplication.getContext(), 5 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)
                    .defaultDisplayImageOptions(getOptions())
                    .build();

            imageLoader.init(config);
        }
        return imageLoader;
    }
 
    public static DisplayImageOptions getOptions(){
        return new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY) // default
                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                .delayBeforeLoading(10)
                .displayer(new SimpleBitmapDisplayer()) // default
                .build();
    }
//
//    public static DisplayImageOptions getRoundImageOptions(){
//        return new DisplayImageOptions.Builder()
//                .showImageForEmptyUri(R.drawable.monkey_round)
//                .showImageOnFail(R.drawable.monkey_round)
//                .resetViewBeforeLoading(true)
//                .cacheInMemory(true)
//                .cacheOnDisc(true)
//                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
//                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
//                .delayBeforeLoading(10)
//                .displayer(new SimpleBitmapDisplayer()) // default
//                .postProcessor(new BitmapProcessor() {
//                    @Override
//                    public Bitmap process(Bitmap bmp) {
//                        return getRoundedBitmap(bmp);
//                    }
//                })
//                .build();
//    }
//
//    public static DisplayImageOptions getRoundImageGreyScaleOptions(){
//        return new DisplayImageOptions.Builder()
//                .showImageForEmptyUri(R.drawable.monkey_round)
//                .showImageOnFail(R.drawable.monkey_round)
//                .resetViewBeforeLoading(true)
//                .cacheInMemory(true)
//                .cacheOnDisc(true)
//                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
//                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
//                .delayBeforeLoading(10)
//                .displayer(new SimpleBitmapDisplayer()) // default
//                .postProcessor(new BitmapProcessor() {
//                    @Override
//                    public Bitmap process(Bitmap bmp) {
//                        return Utils.toGrayscale(getRoundedBitmap(bmp));
//                    }
//                })
//                .build();
//    }
//
//    public static DisplayImageOptions getProfileImageOptions(){
//        return new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.me_profile_pic_mask)
//                .showImageForEmptyUri(R.drawable.me_profile_pic_mask)
//                .showImageOnFail(R.drawable.me_profile_pic_mask)
//                .resetViewBeforeLoading(true)
//                .cacheInMemory(true)
//                .cacheOnDisc(true)
//                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
//                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
//                .delayBeforeLoading(10)
//                .displayer(new SimpleBitmapDisplayer()) // default
//                .postProcessor(new BitmapProcessor() {
//                    @Override
//                    public Bitmap process(Bitmap bmp) {
//                        return getProfileBitmap(bmp);
//                    }
//                })
//                .build();
//
//    }

//    public static Bitmap getRoundedBitmap(Bitmap bitmap) {
//        int w = bitmap.getWidth();
//        int h = bitmap.getHeight();
//
//        int radius = Math.min(h / 2, w / 2);
//        Bitmap output = Bitmap.createBitmap(radius*2 + 8, radius*2 + 8, Bitmap.Config.ARGB_8888);
//
//        Paint p = new Paint();
//        p.setAntiAlias(true);
//
//        Canvas c = new Canvas(output);
//        c.drawARGB(0, 0, 0, 0);
//        p.setStyle(Paint.Style.FILL);
//
//        c.drawCircle(radius + 4, radius + 4, radius, p);
//
//        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
//
//        c.drawBitmap(bitmap, -w/2 + radius +4,-h/2 + radius + 4, p);
//        p.setXfermode(null);
//        p.setStyle(Paint.Style.STROKE);
//        p.setColor(Color.WHITE);
//        p.setStrokeWidth(3);
//        c.drawCircle(radius + 4, radius + 4, radius, p);
//
//        return output;
//    }
//
//    private static final int BITMAP_SIZE = 200;
//    private static Drawable PROFILE_PIC_MASK;
//
////    private static Drawable getPROFILE_PIC_MASK(int size){
////            Resources res = KitcutApplication.getContext().getResources();
////            PROFILE_PIC_MASK = new BitmapDrawable(BitmapFactory.decodeResource(res,R.drawable.me_profile_pic_mask));
////            PROFILE_PIC_MASK.setBounds(0, 0, size, size);
////        return PROFILE_PIC_MASK;
////    }
//
//    public static Bitmap getProfileBitmap(Bitmap bitmap){
//        int w = bitmap.getWidth();
//        int h = bitmap.getHeight();
//        int size = Math.min(w,h);
//        Bitmap output = null;
//        try {
//            output = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
//            Canvas c = new Canvas(output);
//            c.drawBitmap(bitmap,(size - bitmap.getWidth())/2,(size - bitmap.getHeight())/2,new Paint());
//        //    getPROFILE_PIC_MASK(size).draw(c);
//        } catch (Exception e){
//            e.printStackTrace();
//        }
//        return output;
//    }
//
//    public static Bitmap getSquareBitmap(Bitmap bitmap) {
//        int w = bitmap.getWidth();
//        int h = bitmap.getHeight();
//
//        int d = Math.min(h, w);
//        Bitmap output = Bitmap.createBitmap(d, d, Bitmap.Config.ARGB_8888);
//
//        Paint p = new Paint();
//        Canvas c = new Canvas(output);
//        c.drawBitmap(bitmap, -w/2 + d/2,-h/2 + d/2, p);
//
//        return output;
//    }
}