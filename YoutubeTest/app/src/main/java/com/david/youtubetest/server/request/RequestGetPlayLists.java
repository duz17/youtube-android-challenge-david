package com.david.youtubetest.server.request;

import com.android.volley.Request;
import com.david.youtubetest.server.response.ResponseGetPlayLists;

/**
 * request for registration to the bank
 */
public class RequestGetPlayLists extends MyBaseRequest {

    public RequestGetPlayLists() {
    }

    @Override
    public String getUrlFunction(){
        return "hiring/youtube-api.json";
    }

    @Override
    public Class getResponseClass() {
        return ResponseGetPlayLists.class;
    }

    @Override
    public int getType() {
        return Request.Method.GET;
    }
}
