package com.david.youtubetest.viewholder;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.david.youtubetest.R;
import com.david.youtubetest.manager.ImageLoaderManager;
import com.david.youtubetest.model.ListItem;
import com.david.youtubetest.server.Constants;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import org.kaerdan.twolevelexpandablerecyclerview.ViewHolderWithSetter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by SergataDev on 9/2/2017.
 */
public class ListItemViewHolder extends ViewHolderWithSetter<ListItem> implements View.OnClickListener {

    ListItem mListItem;

    TextView mTextView;
    ImageView mImageView;

    public ListItemViewHolder(View itemView) {
        super(itemView);

        mImageView = (ImageView) itemView.findViewById(R.id.image);
        mTextView = (TextView) itemView.findViewById(R.id.text);
        itemView.setOnClickListener(this);
    }

    @Override
    public void setItem(ListItem item) {
        mListItem = item;
        mTextView.setText(item.getTitle());
        ImageLoaderManager.getInstance().displayImage(mListItem.getThumb(), mImageView);
    }

    @Override
    public void onClick(View v) {

        // youtube integration
        Activity activity = (Activity) v.getContext();
        Intent intent = YouTubeStandalonePlayer.createVideoIntent(activity,
                Constants.YOUTUBE_API_KEY, getYoutubeId(mListItem.getLink()));
        activity.startActivity(intent);


        //open native youtube app
//        v.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
//                Uri.parse(mListItem.getLink())));
    }

    private String getYoutubeId(String link) {
        String pattern = "(?:videos\\/|v=)([\\w-]+)";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(link);

        if(matcher.find()){
            return (matcher.group()).substring(2);
        }

        return "";
    }

}
