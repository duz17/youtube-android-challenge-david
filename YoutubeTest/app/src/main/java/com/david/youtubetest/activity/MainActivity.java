package com.david.youtubetest.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.david.youtubetest.R;
import com.david.youtubetest.adapter.PlayListAdapter;
import com.david.youtubetest.manager.DataManager;
import com.david.youtubetest.model.PlayList;
import com.david.youtubetest.server.ServerConnector;
import com.david.youtubetest.server.response.BaseResponse;
import com.david.youtubetest.server.response.ErrorResponse;
import com.david.youtubetest.server.response.ResponseGetPlayLists;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<PlayList> mPlaylists;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.main_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        getData();
    }

    /**
     * fetch the data from manager
     */
    private void getData() {
        DataManager.getInstance().getPlaylists(new ServerConnector.OnResultListener() {
            @Override
            public void onSuccess(BaseResponse baseResponse) {
                ResponseGetPlayLists responseGetPlayLists = (ResponseGetPlayLists) baseResponse;
                mPlaylists = responseGetPlayLists.getData();
                setAdapter();
            }

            @Override
            public void onFailure(ErrorResponse errorResponse) {
            }
        });
    }

    private void setAdapter() {
        mRecyclerView.setAdapter(new PlayListAdapter(mPlaylists));
        mRecyclerView.setHasFixedSize(true);
    }
}
