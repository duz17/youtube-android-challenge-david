package com.david.youtubetest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SergataDev on 9/2/2017.
 */

public class ListItem {

    @SerializedName("Title")
    String title;

    @SerializedName("link")
    String link;

    @SerializedName("thumb")
    String thumb;

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getThumb() {
        return thumb;
    }
}
