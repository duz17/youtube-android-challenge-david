package com.david.youtubetest.viewholder;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import org.kaerdan.twolevelexpandablerecyclerview.ViewHolderWithSetter;

/**
 * Created by SergataDev on 9/2/2017.
 */
public class PlayListViewHolder extends ViewHolderWithSetter<String> {
    TextView mTextView;

    public PlayListViewHolder(View itemView) {
        super(itemView);
        mTextView = (TextView) itemView;
    }

    @Override
    public void setItem(String item) {
        mTextView.setText(item);
    }
}
