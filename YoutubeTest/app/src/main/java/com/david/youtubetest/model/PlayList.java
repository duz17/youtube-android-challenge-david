package com.david.youtubetest.model;

import com.google.gson.annotations.SerializedName;

import org.kaerdan.twolevelexpandablerecyclerview.TwoLevelExpandableAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SergataDev on 9/2/2017.
 */

public class PlayList implements TwoLevelExpandableAdapter.DataSet<String,ListItem> {
    @SerializedName("ListTitle")
    String listTitle;

    @SerializedName("ListItems")
    ArrayList<ListItem> listItems;

    @Override
    public String getData() {
        return listTitle;
    }

    @Override
    public List<ListItem> getChildren() {
        return listItems;
    }
}
