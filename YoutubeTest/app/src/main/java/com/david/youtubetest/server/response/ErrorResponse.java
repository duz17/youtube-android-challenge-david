package com.david.youtubetest.server.response;

import com.android.volley.VolleyError;

/**
 * Created by dors on 2/22/15.
 */
public class ErrorResponse extends VolleyError{

    private int statusCode;

    private String serverRawResponse;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getServerRawResponse() {
        return serverRawResponse;
    }

    public void setServerRawResponse(String serverRawResponse) {
        this.serverRawResponse = serverRawResponse;
    }
}
