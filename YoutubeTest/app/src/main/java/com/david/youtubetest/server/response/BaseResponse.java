package com.david.youtubetest.server.response;

/**
 * Created by Dor on 22/12/2014.
 */
public abstract class BaseResponse {

    int status;
//    String start_time;
//    String end_time;
//    String status_description;

    String jsonString;

    private int httpStatus;

    public static final int ERROR_UNKNOWN = 1234;

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    public String getJsonString() {
        return this.jsonString;
    }
    public void setJsonString(String json) {
        this.jsonString = json;
    }

    public int getStatus() {
        return status;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public void onResponseDoOnBackground() {}
}