package com.david.youtubetest.server.request;

import java.util.Map;
import java.util.TreeMap;

/**
 * Base Request for this application
 */
public abstract class MyBaseRequest extends BaseRequest {

    public static final String BASE_SERVICE_URL = "http://www.razor-tech.co.il/";

    public static String getBaseServiceUrl() {
        return BASE_SERVICE_URL;
    }

    @Override
    public String getServiceUrl() {
        return getBaseServiceUrl();
    }

    @Override
    public Map<String, String> addExtraHeaders() {
        TreeMap<String, String> map = new TreeMap<>();
        return map;
    }
}