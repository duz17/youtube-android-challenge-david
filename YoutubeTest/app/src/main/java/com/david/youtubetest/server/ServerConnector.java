package com.david.youtubetest.server;


import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.david.youtubetest.MyApplication;
import com.david.youtubetest.server.Constants;
import com.david.youtubetest.server.request.BaseRequest;
import com.david.youtubetest.server.request.GsonRequest;
import com.david.youtubetest.server.response.BaseResponse;
import com.david.youtubetest.server.response.ErrorResponse;
import com.google.gson.Gson;

import java.net.HttpURLConnection;

/**
 * helper singelton class for managing server requests
 */
public class ServerConnector {

    private static final String TAG = ServerConnector.class.getSimpleName();
    private static ServerConnector sServerConnector = null;

    public static final int DEFAULT_TIMEOUT = 1000 * 15;
    public static final int DEFAULT_NUMBER_OF_RETRIES = 1;

    private Gson mGson;

    private ServerConnector() {
        mGson = new Gson();
    }

    public static ServerConnector getInstance() {
        if (sServerConnector == null) {
            synchronized (ServerConnector.class) {
                if (sServerConnector == null) {
                    sServerConnector = new ServerConnector();
                }
            }
        }

        return sServerConnector;
    }

    /**
     * add request to queue and process it as soon as possible
     *
     * @param baseRequest      the request containing the json and the url
     * @param onResultListener result listener
     */
    public void processRequest(BaseRequest baseRequest, OnResultListener onResultListener) {
        processRequest(baseRequest, onResultListener, null);
    }

    public void processRequest(final BaseRequest baseRequest, final OnResultListener onResultListener, Object tag) {

        String url = baseRequest.getServiceUrl() + baseRequest.getUrlFunction();
        String body = null;
        final int type = baseRequest.getType();

        switch (type) {
            case Request.Method.POST:
            case Request.Method.PUT:

                body = baseRequest.getEncodedBody();

                if (body == null) {
                    body = mGson.toJson(baseRequest);
                }

                break;
            case Request.Method.GET:
            case Request.Method.DELETE:
            case Request.Method.PATCH:
                url = url.replaceAll(" ", "%20");
                break;
        }

        Log.i(TAG, "AAAAAA processRequest: " + url + "\n" + "with body: " + body);

        GsonRequest gsonRequest = new GsonRequest(
                type,
                url,
                body,
                baseRequest.getBodyContentType(),
                baseRequest.getResponseClass(),
                baseRequest.addExtraHeaders(),
                new Listener<BaseResponse>() {
                    @Override
                    public void onResponse(BaseResponse response) {
                        Log.i(TAG, "AAAAAA onResponse: " + response.getClass().getSimpleName());
                        if (onResultListener != null)
                            onResultListener.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.w(TAG, "AAAAAA onErrorResponse: " + error.toString());
                        ErrorResponse errorResponse = (ErrorResponse) error;

                        if (errorResponse.getStatusCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            Log.i(TAG, "Unauthorized - going to refresh token");
                            // TokenRefresher.doTokenRefresh();
                        }

                        String message = "Error response: " + errorResponse.getStatusCode() + ' ' + errorResponse.getServerRawResponse();
                        if (message.length() > 70) {
                            message = message.substring(0, 70);
                        }
                        message = message.replace('\n', ' ');
                        Log.w(TAG, message);

//                        if (Constants.DEBUG)
//                            Toast.makeText(MyApplication.getContext(), message, Toast.LENGTH_LONG).show();

                        if (onResultListener != null)
                            onResultListener.onFailure(errorResponse);
                    }
                }
        );

        // Set tag
        gsonRequest.setTag(tag == null ? baseRequest.getClass().getSimpleName() : tag);

        // Set retry policy
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(baseRequest.getConnectionTimeout(), baseRequest.getNumberOfRetries(), 0));

        // Add request to queue
        VolleyHelper.getRequestQueue().add(gsonRequest);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     */
    public void cancelPendingRequests(Object tag) {
        RequestQueue requestQueue = VolleyHelper.getRequestQueue();

        if (requestQueue != null && tag != null) {
            requestQueue.cancelAll(tag);
        }
    }

    public interface OnResultListener {
        void onSuccess(BaseResponse baseResponse);

        void onFailure(ErrorResponse errorResponse);
    }

}