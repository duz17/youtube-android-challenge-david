package com.david.youtubetest.server.response;

public class BaseResponseDataArray<T> extends BaseResponse{
    int offset;
    T data[];
    int nextPage;
    int totalObjects;
}
