package com.david.youtubetest.server.request;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.david.youtubetest.server.response.ErrorResponse;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Map;

public class GsonRequest<BaseResponse> extends JsonRequest<BaseResponse> {

    private static final String TAG = GsonRequest.class.getSimpleName();

    public static final String CONTENT_TYPE_X_URL_ENCODED = "application/x-www-form-urlencoded; charset=UTF-8";

    public static final String HTTP_STATUS_KEY = "http_status";

    private final Gson gson;
    private final Class<BaseResponse> clazz;
    private final Map<String, String> headers;
    private final String bodyContentType;
    private final Response.Listener<BaseResponse> listener;
    private final Response.ErrorListener errorListener;

    /**
     * Make a request and return a parsed object from JSON.
     *
     * @param url     URL of the request to make
     * @param clazz   Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     */
    public GsonRequest(
            int method, String url, String body, String bodyContentType, Class<BaseResponse> clazz,
            Map<String, String> headers, Response.Listener<BaseResponse> listener, Response.ErrorListener errorListener)
    {
        super(method, url, body, listener, errorListener);
        this.clazz = clazz;
        this.bodyContentType = bodyContentType;
        this.headers = headers;
        this.listener = listener;
        this.errorListener = errorListener;
        gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    public String getBodyContentType() {
        return bodyContentType != null ? bodyContentType : super.getBodyContentType();
    }

    @Override
    protected Response<BaseResponse> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            json = json.replace("\r\n", "");
            Log.d("Response", json);

            BaseResponse baseResponse;

            // A success response - but has no body - pass an empty base response
            if (TextUtils.isEmpty(json)) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(HTTP_STATUS_KEY, response.statusCode);
                baseResponse = gson.fromJson(jsonObject.toString(), clazz);
                ((com.david.youtubetest.server.response.BaseResponse) baseResponse).onResponseDoOnBackground();
            } else {
                baseResponse = gson.fromJson(json, clazz);
                com.david.youtubetest.server.response.BaseResponse baseResponseObject =
                        (com.david.youtubetest.server.response.BaseResponse) baseResponse;
                baseResponseObject.setJsonString(json);
                baseResponseObject.setHttpStatus(response.statusCode);
                baseResponseObject.onResponseDoOnBackground();
            }

            return Response.success(baseResponse, HttpHeaderParser.parseCacheHeaders(response));
//        } catch (UnsupportedEncodingException e) {
//            return Response.error(new ParseError(e));
//        } catch (JsonSyntaxException e) {
//            return Response.error(new ParseError(e));
        } catch (Exception e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected ErrorResponse parseNetworkError(VolleyError volleyError) {

        ErrorResponse errorResponse = new ErrorResponse();
        try {
            errorResponse.setStatusCode(volleyError.networkResponse.statusCode);
            String error = new String(volleyError.networkResponse.data);
            errorResponse.setServerRawResponse(error);
            Log.i(TAG, "Server error response -> " + error);
            Log.i(TAG, "Status code -> " + volleyError.networkResponse.statusCode);
        } catch (Exception ignored) {
            // Nothing in here
        }

        return errorResponse;
    }

    @Override
    protected void deliverResponse(BaseResponse response) {
        listener.onResponse(response);
    }

    @Override
    public void deliverError(VolleyError error) {

        if (!(error instanceof ErrorResponse)) {
            errorListener.onErrorResponse(new ErrorResponse());
            return;
        }

        errorListener.onErrorResponse(error);
    }
}